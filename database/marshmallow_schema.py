from marshmallow import fields
from marshmallow_sqlalchemy import ModelConversionError, ModelSchema
from schema import *

def setup_paper_schema():
    class InVivoInteractionSchema(ModelSchema):
        in_vivo_interaction_type = fields.Nested(InVivoInteractionType.__marshmallow__)
        animal_type = fields.Nested(AnimalType.__marshmallow__)

    class InVitroInteractionSchema(ModelSchema):
        in_vitro_interaction_type = fields.Nested(InVitroInteractionType.__marshmallow__)
        cell_type = fields.Nested(CellType.__marshmallow__)

    class PaperSchema(ModelSchema):
        applications = fields.Nested(Application.__marshmallow__, many=True)
        authors = fields.Nested(Author.__marshmallow__, many=True)
        cargos = fields.Nested(Material.__marshmallow__, many=True)
        compositions = fields.Nested(Material.__marshmallow__, many=True)
        particle_types = fields.Nested(ParticleType.__marshmallow__, many=True)
        templates = fields.Nested(Template.__marshmallow__, many=True)
        in_vitro_interactions = fields.Nested(InVitroInteractionSchema, many=True)
        in_vivo_interactions = fields.Nested(InVivoInteractionSchema, many=True)
        class Meta:
            model = Paper
    return PaperSchema

def setup_schema(Base, session):
    # Create a function which incorporates the Base and session information
    def setup_schema_fn():
        for class_ in Base._decl_class_registry.values():
            if hasattr(class_, '__tablename__'):
                if class_.__name__.endswith('Schema'):
                    raise ModelConversionError(
                        "For safety, setup_schema can not be used when a"
                        "Model class ends with 'Schema'"
                    )

                class Meta(object):
                    model = class_
                    sqla_session = session

                schema_class_name = '%sSchema' % class_.__name__

                schema_class = type(
                    schema_class_name,
                    (ModelSchema,),
                    {'Meta': Meta}
                )

                setattr(class_, '__marshmallow__', schema_class)
        setattr(Paper, '__marshmallow__', setup_paper_schema())

    return setup_schema_fn

#
# author_schema = AuthorSchema()
#paper_schema = PaperSchema()