from csv import DictReader
import os
import requests
from bs4 import BeautifulSoup
from flask import jsonify
from marshmallow_schema import setup_schema
import json
from schema import *

from sqlalchemy import event
from sqlalchemy.orm import mapper

session = Session()
data_directory = ''


def load_objects_from_csv(csv_file_name, db_class):
    file_name = os.path.join(data_directory, csv_file_name)
    with open(file_name) as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            trim_dict(row)
            session.add(db_class(**row))
    session.commit()


def trim_dict(row):
    for key in row.keys():
        row[key] = row[key].strip()


def get_doi_info(doi):
    url = 'http://dx.doi.org/' + doi
    headers = {'Accept': 'application/vnd.crossref.unixref+xml;q=1, application/rdf+xml;q=0.5'}
    r = requests.get(url, headers=headers)
    return r


def get_paper_info_from_doi(doi):
    r = get_doi_info(doi)
    soup = BeautifulSoup(r.content, "html.parser")
    journal = soup.journal.full_title.text
    title = soup.journal_article.titles.title.text
    try:
        month = soup.journal.month.text
    except:
        month = 1
    year = soup.journal.year.text
    authors = []
    for person in soup.journal_article.contributors.find_all('person_name', contributor_role='author'):
        author = load_or_create_author(person.given_name.text.strip(), person.surname.text.strip())
        authors.append(author)
    paper = Paper(doi=doi, title=title.strip(), journal=journal.strip(), authors=authors, month=month, year=year)
    return paper


def load_or_create_author(given_name, surname):
    author = session.query(Author).filter_by(given_name=given_name, surname=surname).first()
    if author is None:
        author = Author(given_name=given_name, surname=surname)
        session.add(author)
    return author


def load_basic_data():
    load_objects_from_csv('particle_types.csv', ParticleType)
    load_objects_from_csv('templates.csv', Template)
    load_objects_from_csv('materials.csv', Material)
    load_objects_from_csv('cell_types.csv', CellType)
    load_objects_from_csv('in_vitro_interaction_types.csv', InVitroInteractionType)
    load_objects_from_csv('animal_types.csv', AnimalType)
    load_objects_from_csv('in_vivo_interaction_types.csv', InVivoInteractionType)
    load_objects_from_csv('applications.csv', Application)


def load_paper(info):
    try:
        paper = get_paper_info_from_doi(info['doi'])
    except:
        raise ValueError('Didn''t recognize the doi: {0}'.format(info['doi']))
    paper.min_size_nm = info['min_size_nm']
    paper.max_size_nm = info['max_size_nm']
    paper.finding = info['finding']
    add_to_paper(paper, info)
    return paper


def add_to_paper(paper, info):
    find_and_add_paper_attr(paper, info, 'particle_types', ParticleType)
    find_and_add_paper_attr(paper, info, 'templates', Template)
    find_and_add_paper_attr(paper, info, 'compositions', Material)
    find_and_add_paper_attr(paper, info, 'cargos', Material)
    find_and_add_paper_attr(paper, info, 'applications', Application)
    find_and_add_compound_paper_attr(paper, info,
                                     {'in_vitro_interaction_type': InVitroInteractionType, 'cell_type': CellType},
                                     'in_vitro_interactions', PaperInVitroInteraction)
    find_and_add_compound_paper_attr(paper, info,
                                     {'in_vivo_interaction_type': InVivoInteractionType, 'animal_type': AnimalType},
                                     'in_vivo_interactions', PaperInVivoInteraction)


def get_entity_from_name(name, entity_class):
    name = name.strip()
    value = None
    if hasattr(entity_class(), 'abbreviation'):
        value = session.query(entity_class).filter_by(abbreviation=name).first()
    if value is None:
        value = session.query(entity_class).filter_by(name=name).first()
    if value is None:
        raise ValueError(name + ' is an unknown kind of ' + entity_class.__tablename__)
    return value


def find_and_add_paper_attr(paper, info_row, attr, entity_class):
    if info_row[attr] != '':
        entity = get_entity_from_name(info_row[attr], entity_class)
        getattr(paper, attr).append(entity)


def find_and_add_compound_paper_attr(paper, info_row, attrs_to_classes, entity_attr, entity_class):
    attrs = list(attrs_to_classes.keys())
    if info_row[attrs[0]] != '':
        entity = entity_class()
        for attr in attrs:
            sub_entity = get_entity_from_name(info_row[attr], attrs_to_classes[attr])
            setattr(entity, attr, sub_entity)
        getattr(paper, entity_attr).append(entity)


def load_papers():
    file_name = os.path.join(data_directory, 'papers.csv')
    with open(file_name) as csvfile:
        reader = DictReader(csvfile)
        paper = None
        for row in reader:
            if row['doi'].strip() != '':
                paper = load_paper(row)
                session.add(paper)
            else:
                add_to_paper(paper, row)
    session.commit()


def generate_database_from_csv():
    print('Generating database. This may take a few minutes.....')
    load_basic_data()
    load_papers()
    print('Finished generating database')



def generate_json_from_database():
    generate_json_file(Material, 'output/materials.json')
    generate_json_file(Paper, 'output/papers.json')


def generate_json_file(marsh_class, filename):
    all_items = session.query(marsh_class).all()
    schema = marsh_class.__marshmallow__()
    items = [schema.dump(item).data for item in all_items]
    items_container = {'items': items}
    json_string = json.dumps(items_container)
    with open(filename, 'w') as file:
        file.write(json_string)


def generate_papers():
    pass


if __name__ == "__main__":
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    data_directory = 'data'
    paper = get_paper_info_from_doi('10.1021/nl080877c')
    generate_database_from_csv()
    setup_schema(Base, session)()
    generate_json_from_database()
