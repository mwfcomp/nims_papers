from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, sessionmaker
from core import *



dString = String(200)
doiColumn = lambda: Column('doi', dString, ForeignKey('paper.doi'))
id_column = lambda table_name: Column(table_name + '_id', Integer, ForeignKey(table_name + '.id'))
paper_link_table = lambda table_name: Table('paper_' + table_name, Base.metadata, doiColumn(), id_column(table_name))


paper_author = paper_link_table('author')
paper_particle_type = paper_link_table('particle_type')
paper_template = paper_link_table('template')
paper_composition = Table('paper_composition', Base.metadata, doiColumn(), id_column('material'))
paper_cargo = Table('paper_cargo', Base.metadata, doiColumn(), id_column('material'))
# paper_in_vitro_interaction = Table('paper_in_vitro_interaction', Base.metadata, doiColumn(),
#                                     id_column('in_vitro_interaction'), id_column('cell_type'))
# paper_in_vivo_interaction = Table('paper_in_vivo_interaction', Base.metadata, doiColumn(),
#                                   id_column('in_vivo_interaction'), id_column('animal_type'))
paper_application = paper_link_table('application')


class IdNameTableMixin(object):
    id = Column(Integer, primary_key=True)
    name = Column(dString, unique=True)

class Paper(Base):
    __tablename__ = 'paper'
    doi = Column(dString, primary_key=True)
    title = Column(String(500))
    journal = Column(dString)
    authors = relationship("Author", secondary=paper_author)
    particle_types = relationship("ParticleType", secondary=paper_particle_type)
    min_size_nm = Column(Integer)
    max_size_nm = Column(Integer)
    month = Column(Integer)
    year = Column(Integer)
    templates = relationship("Template", secondary=paper_template)
    compositions = relationship("Material", secondary=paper_composition)
    cargos = relationship("Material", secondary=paper_cargo)
    applications = relationship("Application", secondary=paper_application)
    in_vitro_interactions = relationship("PaperInVitroInteraction")
    in_vivo_interactions = relationship("PaperInVivoInteraction")
    finding = Column(String(5000))

class Author(Base):
    __tablename__ = 'author'
    id = Column(Integer, primary_key=True)
    given_name = Column(dString)
    surname = Column(dString)

class ParticleType(IdNameTableMixin, Base):
    __tablename__ = 'particle_type'

class Template(IdNameTableMixin, Base):
    __tablename__ = 'template'

class Material(Base):
    __tablename__ = 'material'
    id = Column(Integer, primary_key=True)
    abbreviation = Column(dString, unique=True)
    name = Column(String(500), unique=True)

class Application(IdNameTableMixin, Base):
    __tablename__ = 'application'

class InVitroInteractionType(IdNameTableMixin, Base):
    __tablename__ = 'in_vitro_interaction_type'

class CellType(IdNameTableMixin, Base):
    __tablename__ = 'cell_type'

class PaperInVitroInteraction(Base):
    __tablename__ = 'paper_in_vitro_interaction'
    doi = Column('doi', dString, ForeignKey('paper.doi'), primary_key=True)
    in_vitro_interaction_type_id = Column(Integer, ForeignKey('in_vitro_interaction_type.id'), primary_key=True)
    cell_type_id = Column(Integer, ForeignKey('cell_type.id'), primary_key=True)
    in_vitro_interaction_type = relationship("InVitroInteractionType")
    cell_type = relationship("CellType")

class InVivoInteractionType(IdNameTableMixin, Base):
    __tablename__ = 'in_vivo_interaction_type'

class AnimalType(IdNameTableMixin, Base):
    __tablename__ = 'animal_type'

class PaperInVivoInteraction(Base):
    __tablename__ = 'paper_in_vivo_interaction'
    doi = Column('doi', dString, ForeignKey('paper.doi'), primary_key=True)
    in_vivo_interaction_type_id = Column(Integer, ForeignKey('in_vivo_interaction_type.id'), primary_key=True)
    animal_type_id = Column(Integer, ForeignKey('animal_type.id'), primary_key=True)
    in_vivo_interaction_type = relationship("InVivoInteractionType")
    animal_type = relationship("AnimalType")

if __name__ == "__main__":
    session = Session()
    Base.metadata.create_all(engine)

    test_paper = Paper(doi='10.32141/reti', title='Particles and capsules', journal='Nature')
    pmash = Material(abbreviation='PMASH', name='thiol-functionalized poly(methacrylic acid)')
    uptake = InVitroInteractionType(name='Uptake')
    hela = CellType(name='HeLa')
    test_paper_interact =PaperInVitroInteraction(interaction_type=uptake, cell_type=hela)
    test_paper.in_vitro_interactions = [test_paper_interact]
    test_paper.materials = [pmash]
    session.add(test_paper)
    session.commit()
    print('hello!')
