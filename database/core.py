from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
in_memory_db_connection_string = 'sqlite:///:memory:'
on_disk_db_connection_string = 'sqlite:///paper.db'

engine = create_engine(on_disk_db_connection_string, echo=False)
Base = declarative_base()
Session = sessionmaker(bind=engine)
