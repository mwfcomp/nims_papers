from flask import Flask
from database.schema import *
from database.core import Session
from flask import jsonify, send_from_directory
import database.load_data as db_loader
from database.marshmallow_schema import setup_schema

app = Flask(__name__, static_folder='frontend/static')
db_loaded = False
session = None

# @app.route("/papers")
# def test():
#     all_papers = session.query(Paper).all()
#     paper_schema = Paper.__marshmallow__()
#     items = [paper_schema.dump(paper).data for paper in all_papers]
#     items_container = {'items': items}
#     return permissive_json_response(items_container)
#     # d = marshmallow_schema.author_schema.dump(a[0]).data
#     # return jsonify(marshmallow_schema.paper_schema.dump(a[0]))
#
#
# @app.route("/materials")
# def materials():
#     all_materials = session.query(Material).all()
#     material_schema = Material.__marshmallow__()
#     items = [material_schema.dump(material).data for material in all_materials]
#     items_container = {'items': items}
#     return permissive_json_response(items_container)
#
#
# def permissive_json_response(py_object):
#     # jsonify doesn''t do lists!
#     json_response = jsonify(py_object)
#     json_response.headers['Access-Control-Allow-Origin'] = '*'
#     return json_response
#
#
# @app.route("/sample-data")
# def sample_data():
#     a = lambda: None
#     a.title = 'test'
#     sample_data = [a]
#     # sample_data = {title: 'test'}
#     return permissive_json_response(sample_data)


@app.route("/")
def hello():
    return "you want /table!"

@app.route("/table")
def send_base():
    return send_from_directory(app.static_folder, 'index.html')

@app.route("/<path:path>")
def send_static(path):
    return send_from_directory('frontend/static', path)


if __name__ == "__main__":
    session = Session()
    setup_schema(Base, session)()
    app.run(port=8052, debug=True, host='0.0.0.0')
