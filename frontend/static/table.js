$(function() {
    console.log('Loaded!');
    paper_table_data_url = "data/papers.json";
    material_abbrev_data_url = "data/materials.json";
    paper_table_s = '.paper-table';
    paper_table_body_s = paper_table_s + ' tbody';
    material_table_s = '.material-table';
    material_table_body_s = material_table_s + ' tbody';
    paper_table_row_s = paper_table_s + ' tbody tr';
    doi_base_url = 'http://dx.doi.org/';
    paper_headers_to_cell_info = {
        "Title" : title_entry_cell_info(),
        "Journal": single_entry_cell_info("journal"),
        "Year" :  single_entry_cell_info("year"),
//        "Authors" : named_multi_entry('authors'),
        "Authors" : multi_entry_cell_info("authors", author_cell_text),
//        "Authors": {'cell_fn': author_cell_function, 'class': 'authors collapsing'},
        "Particle Types": named_multi_entry('particle_types'),
        "Templates": named_multi_entry('templates'),
        "Compositions": abbrv_multi_entry('compositions'),
        "Cargos" : abbrv_multi_entry('cargos'),
        "Size Range (nm)": {'cell_fn': size_range_cell_fn, 'class': 'size_range collapsing'},
        "In Vivo": checked_cell_with_popup_info(in_vivo_multi_entry()),
        "In Vitro": checked_cell_with_popup_info(in_vitro_multi_entry()),
        "Applications": named_multi_entry('applications')
    };
    material_headers_to_cell_info = {
            "Abbreviation": single_entry_cell_info('abbreviation'),
            'Full name': single_entry_cell_info('name')
    };
    searched_headers = ['Journal', 'Year', 'Particle Types', 'Templates', 'Compositions', 'Cargos', 'Applications', 'Authors'];
    $.fn.api.settings.debug = true;
    create_table_header(paper_headers_to_cell_info, paper_table_s);
    create_table_header(material_headers_to_cell_info, material_table_s);

    $.getJSON(
        paper_table_data_url,
        handle_paper_response);

    $.getJSON(
        material_abbrev_data_url,
        handle_material_response);

    //fix tablesort breaking popups
    $('.paper-table').on('tablesort:complete', add_popups)

    $('.paper-table').tablesort();
    $('.ui.accordion').accordion();

});

add_popups = function() {$('.has-popup').popup();};

create_table_header = function (headers_to_cell_info, table_s) {
    $('<thead><tr></tr></thead>').appendTo(table_s)
    table_head_row_s = table_s + ' thead tr';
    header_texts = Object.keys(headers_to_cell_info);
    header_texts.map( function(header_text) {
        header = $('<th></th>');
        header.addClass(header_text + "_header")
        header.text(header_text);
        header.appendTo(table_head_row_s);
    });
}

all_papers = null;
handle_paper_response = function(all_paper_json) {
    all_papers = all_paper_json.items;
    populate_table(all_papers, paper_headers_to_cell_info, paper_table_body_s);
    setup_search();
    add_popups();
    $('.loading-overlay').hide();
};

all_materials = null;
handle_material_response = function(all_materials_json) {
    all_materials = all_materials_json.items;
    populate_table(all_materials, material_headers_to_cell_info, material_table_body_s);
 };

populate_table = function(items, headers_to_cell_info, table_body_s) {
    items.map(function(item) {
        create_table_row(item, headers_to_cell_info, table_body_s);
    });
};

setup_search = function() {
    to_search = [];
    searched_headers.map(function(header) {
        all_rows = $(paper_table_row_s);
        row_to_search_terms = paper_headers_to_cell_info[header].row_to_search_terms_fn;
        search_terms = $.map(all_rows, row_to_search_terms);
        $.merge(to_search, search_terms);
    });
    to_search = _.uniq(to_search, _.iteratee('title'));

    handle_search = function(search_term, resp) {
        all_rows = $(paper_table_row_s);
        all_rows.hide();
        all_rows.toArray().map(function(row) {
            searched_headers.map(function(header) {
                row_to_search_terms = paper_headers_to_cell_info[header].row_to_search_terms_fn;
                search_terms = row_to_search_terms(row);
                search_terms = $.makeArray(search_terms);
                search_terms.map(function(term) {
                    if (term.title == search_term.title) {
                        $(row).show();
                    }
                });
            });
        });
    };

    $('.ui.search').search({
        source : to_search,
        searchFullText: true,
        maxResults: 20,
        onSelect: handle_search
    });

    $('.ui.clear-search').click(function() {
        $(paper_table_row_s).show();
        $('.ui.search input')[0].value = '';
    });
};

create_table_row = function(item, headers_to_cell_info, table_body_s) {
    row = $('<tr></tr>');
    Object.keys(headers_to_cell_info).map(function(header) {
        cell_info = headers_to_cell_info[header]
        cell_text_fn = cell_info.cell_fn;
        cell = $('<td></td>');
        cell.addClass(cell_info.class);
        cell_text_fn(cell, item);

        if (cell_info.popup_fn != undefined) {
            popup = $('<div></div>');
            cell_info.popup_fn(popup, item);
            cell.attr('data-html', popup.prop('outerHTML'));
            cell.attr('data-variation', 'inverted');
        }
        cell.appendTo(row);
    });
    row.appendTo(table_body_s);
};

title_entry_cell_info = function() {
    cell_info = single_entry_cell_info('Title');
    linked_cell_fn = function(cell, paper_info) {
        doi_link = $('<a></a>');
        full_doi_url = doi_base_url + paper_info.doi;
        doi_link.attr('href', full_doi_url);
        doi_link.attr('target', '_blank');
        doi_link.text(paper_info.title);
        cell.append(doi_link);
        cell.addClass('has-popup');
    };
    cell_info.cell_fn = linked_cell_fn;

    cell_info.popup_fn = function(popup, paper_info) {
        author_fn = multi_entry_cell_info('authors', author_cell_text).cell_fn;
        author_fn(popup, paper_info);
    };


    return cell_info;
};

single_entry_cell_info = function(name) {
    cell_function = function(cell, paper_info) {
        cell.text(paper_info[name]);
        return;
    };
    row_to_search_terms_fn = function(row) {
        selector = '.' + name;
        return {'title' : $(selector, row).text()};
    };

    return {'cell_fn': cell_function,
            'class': name,
            'row_to_search_terms_fn': row_to_search_terms_fn};
};

multi_entry_cell_info = function(property, property_text_fn) {
    cell_function = function(cell, paper_info) {
        all_items = paper_info[property];
        all_items.map(function(item) {
            container = $('<div></div>');
            text = property_text_fn(item);
            container.append(text);
            cell.append(container);
        });
    }
    row_to_search_terms_fn = function(row) {
        property_selector = '.'+property + ' div';
        return $.map($(property_selector, row), function(div) {
            return {'title': $(div).text()};
        });
    };
    cell_info = {'cell_fn': cell_function,
        'class': property + ' ', //used to say collapsing
        'row_to_search_terms_fn': row_to_search_terms_fn};
    return cell_info;
};

named_multi_entry = function(property) {
    return multi_entry_cell_info(property, function(e) {return e.name;});
};

abbrv_multi_entry = function(property) {
    return multi_entry_cell_info(property, function(e) {return e.abbreviation;});
};

in_vitro_multi_entry = function() {
    in_vitro_cell_text = function(interaction) {
        text = interaction.in_vitro_interaction_type.name + " (" + interaction.cell_type.name + ")";
        return text;
    };
    return multi_entry_cell_info('in_vitro_interactions', in_vitro_cell_text);
};
in_vivo_multi_entry = function() {
    in_vivo_cell_text = function(interaction) {
        text = interaction.in_vivo_interaction_type.name + " (" + interaction.animal_type.name + ")";
        return text;
    };
    return multi_entry_cell_info('in_vivo_interactions', in_vivo_cell_text)
};

checked_cell_with_popup_info = function(base_cell_info) {
    checked_cell_fn = function(cell, paper_info) {
        base_cell_info.cell_fn(cell, paper_info);
        if (cell.text().length > 0) {
            cell.empty();
            icon = $('<i class="large plus square icon"></i>');
            sort_div = $('<div class="hidden-sortable">a</div>');
            cell.append(icon).append(sort_div);
            cell.addClass('has-popup');
        } else {
            cell.empty();
        }
    };
    popup_fn = function(popup, paper_info) {
        base_cell_info.cell_fn(popup, paper_info);
    }

    return {'cell_fn': checked_cell_fn,
            'class': base_cell_info.class,
            'row_to_search_terms_fn': base_cell_info.row_to_search_terms_fn,
            'popup_fn': popup_fn};
};

author_cell_text = function(author) {
    full_name = author.given_name + " " + author.surname;
    return full_name;
};

size_range_cell_fn = function(cell, paper_info) {
    size_range = "" + paper_info.min_size_nm + "-" + paper_info.max_size_nm;
    cell.text(size_range);
    return;
};
